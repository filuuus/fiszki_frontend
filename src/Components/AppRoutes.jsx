import React from "react";
import { Redirect, Route } from "react-router-dom";

import { useAuthState } from "../Context";
import Layout from "./Layout";

const AppRoutes = ({ component: Component, path, isPrivate, ...rest }) => {
  const userDetails = useAuthState();
  return (
    <Route
      path={path}
      render={(props) =>
        isPrivate ? (
          !Boolean(userDetails.token) ? (
            <Redirect to={{ pathname: "/" }} />
          ) : (
            <Layout>
               <Component {...props} />
            </Layout>
          )
        ) : Boolean(userDetails.token) ? (
          <Redirect to={{ pathname: "/dashboard" }} />
        ) : (
          <Component {...props} />
        )
      }
      {...rest}
    />
  );
};

export default AppRoutes;
