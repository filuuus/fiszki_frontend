import './App.css';
// eslint-disable-next-line
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import routes from './Config/routes.js';
import { AuthProvider } from "./Context";
import AppRoute from './Components/AppRoutes';

function App() {
  return (
    <AuthProvider>
      <BrowserRouter>
        <Switch>
          {routes.map((route) => (
            <AppRoute
              key={route.path}
              path={route.path}
              isPrivate={route.isPrivate}
              component={route.component}
              exact={route.exact}
            />
          ))}
        </Switch>
      </BrowserRouter>
    </AuthProvider>
  );
}

export default App;