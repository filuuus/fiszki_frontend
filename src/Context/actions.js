const ROOT_URL = 'http://fiszki.local/api';
 
export async function loginUser(dispatch, loginPayload) {
  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(loginPayload),
  };

  const csrfReqyestOption = {
    method: 'GET',
    headers: { 'Content-Type': 'application/json' },
  }
 
  try {
    dispatch({ type: 'REQUEST_LOGIN' });
    // eslint-disable-next-line
    let csrfCookie = await fetch(`http://fiszki.local/sanctum/csrf-cookie`, csrfReqyestOption);
    let response = await fetch(`${ROOT_URL}/login`, requestOptions);
    let data = await response.json();
 
    if (data.user) {
      dispatch({ type: 'LOGIN_SUCCESS', payload: data });
      localStorage.setItem('currentUser', JSON.stringify(data));
      return data
    }
 
    dispatch({ type: 'LOGIN_ERROR', error: 'error' });
    return;
  } catch (error) {
    dispatch({ type: 'LOGIN_ERROR', error: error });
  }
}
 
export async function logout(dispatch) {
  dispatch({ type: 'LOGOUT' });
  localStorage.removeItem('currentUser');
  localStorage.removeItem('token');
  console.log('log out');
}