import Login from '../Pages/Login/index';
import Register from '../Pages/Register/index';
import Dashboard from '../Pages/Dashboard/index';
import Flashcards from '../Pages/Flashcards/index';
import PageNotFound from '../Pages/NotFound/index';

const routes =[
    {
      path:'/',
      component: Login,
      isPrivate: false,
      exact: true,
    },
    {
      path:'/register',
      component: Register,
      isPrivate: false,
    },
    {
      path:'/dashboard',
      component: Dashboard,
      isPrivate: true,
    },
    {
      path:'/flashcards',
      component: Flashcards,
      isPrivate: true,
    },
    {
      path:'/*',
      component: PageNotFound,
      isPrivate: true,
    }
  ]
   
  export default routes